module.exports = {
    fields: {
        nome            : 'text',
        email           : 'text',
        password_hash   : 'blob'
    },
    key: ['nome']
    // methods: {
    //     setPassword: function (password, callback) {
    //       crypto.pbkdf2Sync(password, 'salt', 100000, 512, 'sha512', function(err, hashed) {
    //         if (err) { return callback(err); }
    //         this.password_hash = hashed;
    //         return callback();
    //       });
        // }
    // }
}