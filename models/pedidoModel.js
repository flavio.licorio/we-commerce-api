module.exports = {
    fields: {
        id          : 'text',
        status      : 'boolean',
        quantidade  : 'int',
        preco       : 'float',
        produto     : 'text',
        usuario     : 'text'
    },
    key: ['id','usuario']
}