module.exports = {
    fields: {
        nome    : 'text',
        preco   : 'float',
        estoque : 'int'
    },
    key: [['nome']]
}