const express = require('express');
const app = express();
const morgan = require('morgan');
const cassandra = require('express-cassandra');

var cors = require('cors');

const routeProdutos = require('./routes/produtos-routes');
const routePedidos = require('./routes/pedidos-routes');
const routeUsuarios = require('./routes/usuarios-routes');

app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

cassandra.setDirectory(__dirname + '/models').bind(
    {
        clientOptions: {
            contactPoints: ['127.0.0.1'],
            protocolOptions: { port: 9042 },
            keyspace: 'wecommerced',
            queryOptions: { consistency: cassandra.consistencies.one }
        },
        ormOptions: {
            defaultReplicationStrategy: {
                class: 'SimpleStrategy',
                replication_factor: 1
            },
            migration: 'safe'
        }
    },
    function (err) {
        if (err) { console.log(err); }
    }
);


app.use('/produtos', routeProdutos);

app.use('/pedidos', routePedidos);

app.use('/usuarios', routeUsuarios);

app.use((req, res, next) => {
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        mensagem: error.message
    });
});

module.exports = app;
