const express = require('express');
const router = express.Router();
var models = require('express-cassandra');
const uuid = require('uuid/v1');
const crypto = require('crypto')

router.get('/', (req, res, next) => {
    models.instance.usuario.find({}, function(err,result){
        if(err) {
            console.log(err);
            return;
        }

        usuarios = []
        result.forEach(usuario => {
            usuarios.push({
                nome            : usuario.nome,
                email           : usuario.email,
                password_hash   : usuario.password_hash
            });
        });
        return res.status(200).send(usuarios);
    });
});

router.get('/:nome', (req, res, next) => {
    models.instance.usuario.findOne({nome: req.params.nome}, {allow_filtering : true}, function(err, result){
        if(err) {
            console.log(err);
            return;
        }
        return res.status(200).send(result);
    });
});

router.post('/', (req, res, next) => {
    req.body.nome.replace(' ', '-')

    var password = req.body.senha;
    var password_hash = crypto.pbkdf2Sync(password,'we-commerced-setPassword',100000,512, 'sha512',
        function(err, hashed) {
            if (err) { return callback(err); }
            return hashed;
        });
    models.instance.usuario.findOne({nome: req.body.nome}, (err, usuario)=> {
        console.log(password_hash)
        if(usuario){
            usuario.email = req.body.email;
            usuario.password_hash = password_hash;
        } else {
            usuario = new models.instance.usuario({
                nome            : req.body.nome,
                email           : req.body.email,
                password_hash   : password_hash
            });
            //  TODO: setPassword
        }
        usuario.save((err)=> {
            if(err) {
                res.status(500).send({ error: err });
            } else {
                res.status(201).send({
                    message : "Sucesso! Usuario inserido/alterado",
                    result : usuario
                });
            }
        });
    });
});

router.post('/login', (req, res, next) => {
    var password = req.body.senha;
    var password_hash = crypto.pbkdf2Sync(password,'we-commerced-setPassword',100000,512, 'sha512',
        function(err, hashed) {
            if (err) { return callback(err); }
            return hashed;
        });
    models.instance.usuario.findOne({ email: req.body.email, password_hash: password_hash}, {allow_filtering: true}, function(err, result){
        if(err) { console.log(err); return; }
        if (!result) {
            res.status(401).send({
                mensagem: "Falha na autenticação"
            });
        } else {
            res.status(200).send(result);
        }
    });
});

router.delete('/', (req,res,next) => {
    models.instance.usuario.delete({nome : req.body.nome}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Usuario removido!"
        });
    });
});

module.exports = router;