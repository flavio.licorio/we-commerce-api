const express = require('express');
const router = express.Router();
var models = require('express-cassandra');
const uuid = require('uuid/v1');

router.get('/', (req, res, next) => {
    models.instance.pedido.find({}, function(err,result){
        if(err) {
            console.log(err);
            return;
        }

        pedidos = []
        result.forEach(pedido => {
            pedidos.push({
                id          : pedido.id,
                status      : pedido.status,
                quantidade  : pedido.quantidade,
                preco       : pedido.preco,
                produto     : pedido.produto,
                usuario     : pedido.usuario
            });
        });
        return res.status(200).send(pedidos);
    });
});

router.get('/:id', (req, res, next) => {
    models.instance.pedido.findOne({id: req.params.id}, {allow_filtering : true}, function(err, result){
        if(err) {
            console.log(err);
            return;
        }
        return res.status(200).send(result);
    });
});

// todos produtos do usuario
router.get('/usuario/:usuario', (req, res, next) => {
    models.instance.pedido.find({usuario: req.params.usuario}, {allow_filtering : true}, function(err, result){
        if(err) {
            console.log(err);
            return;
        }
        return res.status(200).send(result);
    });
});

router.get('/produto/:produto', (req, res, next) => {
    models.instance.pedido.find({produto: req.params.produto}, {allow_filtering : true}, function(err, result){
        if(err) {
            console.log(err);
            return;
        }
        return res.status(200).send(result);
    });
});

router.post('/', (req, res, next) => {
    models.instance.pedido.findOne({id: req.body.id}, (err, pedido)=> {
        if(pedido){
            pedido.status = Boolean(req.body.status);
            pedido.quantidade = req.body.quantidade;
            pedido.preco = req.body.preco;
            pedido.produto = req.body.produto;
            pedido.usuario = req.body.usuario;
        } else {
            pedido = new models.instance.pedido({
                id          : uuid(),
                status      : Boolean(req.body.status),
                quantidade  : req.body.quantidade,
                preco       : req.body.preco,
                produto     : req.body.produto,
                usuario     : req.body.usuario
            });
        }
        pedido.save((err)=> {
            if(err) {
                res.status(500).send({ error: err });
            } else {
                res.status(201).send({
                    message : "Sucesso! Pedido inserido/alterado",
                    result : pedido
                });
            }
        });
    });
});

router.delete('/', (req,res,next) => {
    models.instance.pedido.delete({id : req.body.id}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Pedido removido!"
        });
    });
});

module.exports = router; 