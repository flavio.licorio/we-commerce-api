const express = require('express');
const router = express.Router();
const uuid = require('uuid/v1');
var models = require('express-cassandra')

router.get('/', (req, res, next) => {
    models.instance.produto.find({}, function(err,result){
        if(err) {
            console.log(err);
            return;
        }

        produtos = []
        result.forEach(produto => {
            produtos.push({
                nome: produto.nome,
                preco: produto.preco,
                estoque: produto.estoque
            });
        });
        return res.status(200).send(produtos);
    });
});

router.get('/:nome', (req, res, next) => {
    models.instance.produto.findOne({nome: req.params.nome}, {allow_filtering : true}, function(err, result){
        if(err) {
            console.log(err);
            return;
        }
        return res.status(200).send(result);
    });
});

router.post('/', (req, res, next) => {
    models.instance.produto.findOne({nome: req.body.nome}, (err, produto)=> {
        if(produto){
            produto.preco = req.body.preco;
            produto.estoque = req.body.estoque;
        } else {
            produto = new models.instance.produto({
                nome    : req.body.nome,
                preco   : req.body.preco,
                estoque : req.body.estoque 
            });
        }
        produto.save((err)=> {
            if(err) {
                res.status(500).send({ error: err });
            } else {
                res.status(201).send({
                    message : "Sucesso! Produto inserido/alterado",
                    result : produto
                });
            }
        });
    });

});

router.delete('/', (req,res,next) => {
    models.instance.produto.delete({nome : req.body.nome}, function(err, result){
        if(err) { console.log(err); return; }
        return res.status(201).send({
            mensagem: "Produto removido!"
        });
    });
});

module.exports = router;